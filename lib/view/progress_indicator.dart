import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyProgressIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
              data: Theme.of(context).copyWith(accentColor: Colors.blue[300]),
              child: new CircularProgressIndicator(),
            );
  }
  
}