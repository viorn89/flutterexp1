import 'package:cached_network_image/cached_network_image.dart';
import 'package:fexper/model/post.dart';
import 'package:fexper/view/progress_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

//Builder виджета поста в списке
IndexedWidgetBuilder postListBuilder(List<Post> models) {
  return (context, index) {
    Post item = models[index];
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 180,
      child: Card(
          elevation: 1,
          color: Colors.white,
          child: Container(
            padding: EdgeInsets.all(6),
            child: Row(
              children: [
                Column(
                  children: [
                    Container(
                        width: 80,
                        child: CachedNetworkImage(
                          imageUrl:
                              item.imageURL == null ? "null" : item.imageURL,
                          placeholder: (context, url) => Center(
                              child: MyProgressIndicator()),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        )),
                    Spacer()
                  ],
                ),
                VerticalDivider(width: 8),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      item.title,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 21,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      item.body,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 7,
                      style: TextStyle(color: Colors.grey[850], fontSize: 14),
                    )
                  ],
                ))
              ],
            ),
          )),
    );
  };
}
