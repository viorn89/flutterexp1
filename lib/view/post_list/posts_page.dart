import 'package:fexper/bloc/post_list/post_list_bloc.dart';
import 'package:fexper/bloc/post_list/post_list_event.dart';
import 'package:fexper/bloc/post_list/post_list_state.dart';
import 'package:fexper/view/post_list/post_list_builder.dart';
import 'package:fexper/view/progress_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PostsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PostListState();
  }
}

class _PostListState extends State<PostsPage> {
  ScrollController _scrollController = new ScrollController();

  Function _paginationNextPage;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<PostListBloc>(context).add(LoadFirstPage());
    return Scaffold(
      body: Container(
        color: Colors.grey[200],
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child:
            BlocBuilder<PostListBloc, PostListState>(builder: (context, state) {
          if (state.loading == true) {
            return Center(child: MyProgressIndicator());
          } else if (state.posts != null) {
            //Удаляем старый обработчик пагинации
            _scrollController.removeListener(_paginationNextPage);
            //Создаем обработчик пагинации
            _paginationNextPage = () {
              if (_scrollController.position.pixels ==
                  _scrollController.position.maxScrollExtent) {
                BlocProvider.of<PostListBloc>(context).add(LoadNextPage());
              }
            };
            //Добавляем пагинацию (если требуеться)
            if (state.pagination)
              _scrollController.addListener(_paginationNextPage);

            //Создаем билдер элеменнтов списка
            final postBuilder = postListBuilder(state.posts);
            return ListView.builder(
                controller: _scrollController,
                itemCount: state.posts.length + 1,
                itemBuilder: (context, index) {
                  if (index == state.posts.length) {
                    //Если можно пагинировать, показывает прелоадер
                    if (state.pagination) {
                      return Container(
                        padding: EdgeInsets.only(top: 8, bottom: 8),
                        child: Center(child: MyProgressIndicator()),
                      );
                    } else {
                      return Container();
                    }
                  } else {
                    return postBuilder(context, index);
                  }
                });
          } else {
            return Container();
          }
        }),
      ),
    );
  }
}
