import 'package:print_color/print_color.dart';

abstract class Log {
  static e(String message, [StackTrace stack]) {
    Print.red("${DateTime.now()} ERROR\n$message");
    if (stack != null) Print.red("$stack");
  }

  static d(String message) {
    Print.green("${DateTime.now()} DEBUG\n$message");
  }
}
