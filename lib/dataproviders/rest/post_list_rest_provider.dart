import 'dart:convert';

import 'package:fexper/dataproviders/rest/http_client.dart';
import 'package:fexper/dataproviders/rest/responses/post_list_response.dart';
import 'package:fexper/main.dart';

class PostListRestDataProvider {
  Future<PostListResponse> getPostList(int offsetId) async {
    final url = baseUrl +
        (offsetId != null ? "/posts/list?offsetId=$offsetId" : "/posts/list");
    final response = await HttpClient.get(url).timeout(Duration(seconds: 3));
    final responseJson = json.decode(response.body) as Map<String, dynamic>;
    return PostListResponse.fromJson(responseJson);
  }
}
