import 'dart:convert';

import 'package:fexper/log.dart';
import 'package:http/http.dart' as http;
import 'package:print_color/print_color.dart';
import 'package:uuid/uuid.dart';

abstract class HttpClient {
    static Future<http.Response> get(url, {Map<String, String> headers}) async {
      var uuid = Uuid().v1();
      Log.d("REQUEST $uuid\nGET:$url\nHEADERS\n$headers");
      final response = await http.get(url, headers: headers);
      Log.d("RESPONSE $uuid\nGET:$url\nHEADERS\n$headers\nSTATUS:${response.statusCode}\nBODY\n${response.body}");
      return response;
    }

    static Future<http.Response> post(url,
        {Map<String, String> headers, body, Encoding encoding}) async {
      var uuid = Uuid().v1();
      Log.d("REQUEST $uuid\nPOST:$url\nHEADERS\n$headers");
      final response = await http.post(url, headers: headers, body: body, encoding: encoding);
      Log.d("RESPONSE $uuid\nPOST:$url\nHEADERS\n$headers\nSTATUS:${response.statusCode}\nBODY\n${response.body}");
      return response;
    }
}