import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:fexper/model/post.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'post_list_response.g.dart';

@immutable
@CopyWith()
@JsonSerializable(nullable: false)
class PostListResponse {
  final List<Post> list;
  final int lastId;
  PostListResponse({this.list, this.lastId});

  factory PostListResponse.fromJson(Map<String, dynamic> json) =>
      _$PostListResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PostListResponseToJson(this);
}
