// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_list_response.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PostListResponseCopyWithExtension on PostListResponse {
  PostListResponse copyWith({
    int lastId,
    List<Post> list,
  }) {
    return PostListResponse(
      lastId: lastId ?? this.lastId,
      list: list ?? this.list,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostListResponse _$PostListResponseFromJson(Map<String, dynamic> json) {
  return PostListResponse(
    list: (json['list'] as List)
        .map((e) => Post.fromJson(e as Map<String, dynamic>))
        .toList(),
    lastId: json['lastId'] as int,
  );
}

Map<String, dynamic> _$PostListResponseToJson(PostListResponse instance) =>
    <String, dynamic>{
      'list': instance.list,
      'lastId': instance.lastId,
    };
