import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:fexper/model/post.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'post_list_state.g.dart';

@immutable
@JsonSerializable(nullable: false)
@CopyWith(generateCopyWithNull: true)
class PostListState {
  final bool loading;
  final String error;
  final List<Post> posts;
  final bool pagination;

  PostListState(
      {this.posts, this.pagination = false, this.loading = false, this.error});

  factory PostListState.fromJson(Map<String, dynamic> json) =>
      _$PostListStateFromJson(json);
  Map<String, dynamic> toJson() => _$PostListStateToJson(this);
}
