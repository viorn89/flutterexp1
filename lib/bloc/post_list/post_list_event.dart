import 'package:flutter/foundation.dart';

@immutable
abstract class PostListEvent {}

class LoadFirstPage extends PostListEvent {}

class LoadNextPage extends PostListEvent {}
