// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_list_state.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PostListStateCopyWithExtension on PostListState {
  PostListState copyWith({
    String error,
    bool loading,
    bool pagination,
    List<Post> posts,
  }) {
    return PostListState(
      error: error ?? this.error,
      loading: loading ?? this.loading,
      pagination: pagination ?? this.pagination,
      posts: posts ?? this.posts,
    );
  }

  PostListState copyWithNull({
    bool error = false,
    bool loading = false,
    bool pagination = false,
    bool posts = false,
  }) {
    return PostListState(
      error: error == true ? null : this.error,
      loading: loading == true ? null : this.loading,
      pagination: pagination == true ? null : this.pagination,
      posts: posts == true ? null : this.posts,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostListState _$PostListStateFromJson(Map<String, dynamic> json) {
  return PostListState(
    posts: (json['posts'] as List)
        .map((e) => Post.fromJson(e as Map<String, dynamic>))
        .toList(),
    pagination: json['pagination'] as bool,
    loading: json['loading'] as bool,
    error: json['error'] as String,
  );
}

Map<String, dynamic> _$PostListStateToJson(PostListState instance) =>
    <String, dynamic>{
      'loading': instance.loading,
      'error': instance.error,
      'posts': instance.posts,
      'pagination': instance.pagination,
    };
