import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:fexper/log.dart';
import 'package:fexper/repositories/post_list/abstract.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:fexper/bloc/post_list/post_list_state.dart';
import 'package:fexper/bloc/post_list/post_list_event.dart';

class PostListBloc extends Bloc<PostListEvent, PostListState>
    with HydratedMixin {
  PostListRepository postListRepository;
  PostListBloc(this.postListRepository) : super(PostListState(loading: true));

  @override
  Stream<PostListState> mapEventToState(
    PostListEvent event,
  ) async* {
    if (event is LoadFirstPage) {
      yield* loadFirstPage();
    } else if (event is LoadNextPage) {
      yield* loadNextPage();
    }
  }

  Stream<PostListState> loadFirstPage() async* {
    try {
      yield PostListState(loading: true);
      final posts = await postListRepository.getPosts();
      yield PostListState(
          posts: posts.list, pagination: posts.lastId != posts.list.last.id);
    } catch (e, s) {
      Log.e(e.toString(), s);
      yield PostListState(error: e.toString());
    }
  }

  Stream<PostListState> loadNextPage() async* {
    try {
      final mState = state;
      if (mState.posts != null && mState.posts.length > 0) {
        final posts =
            await postListRepository.getPosts(offsetId: mState.posts.last.id);
        yield PostListState(
            posts: mState.posts + posts.list,
            pagination: posts.lastId != posts.list.last.id);
      } else {
        yield* loadFirstPage();
      }
    } catch (e, s) {
      Log.e(e.toString(), s);
      yield PostListState(error: e.toString());
    }
  }

  @override
  PostListState fromJson(Map<String, dynamic> json) {
    try {
      return PostListState.fromJson(json);
    } catch (e) {
      return PostListState(loading: true);
    }
  }

  @override
  Map<String, dynamic> toJson(PostListState state) {
    return state.toJson();
  }
}
