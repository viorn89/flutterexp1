import 'package:fexper/bloc/post_list/post_list_bloc.dart';
import 'package:fexper/repositories/post_list/abstract.dart';
import 'package:fexper/view/post_list/posts_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

const String baseUrl = "http://192.168.1.69:8080";

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HydratedBloc.storage = await HydratedStorage.build();
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider<PostListBloc>(
        create: (BuildContext context) =>
            PostListBloc(PostListRepository.instance()),
      ),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      builder: (context, child) {
        return PostsPage();
      },
    );
  }
}
