import 'dart:convert';

import 'package:fexper/dataproviders/rest/post_list_rest_provider.dart';
import 'package:fexper/dataproviders/rest/responses/post_list_response.dart';
import 'package:fexper/log.dart';
import 'package:fexper/main.dart';
import 'package:fexper/model/post.dart';
import 'package:fexper/repositories/post_list/abstract.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:print_color/print_color.dart';

class PostListRepositoryImpl extends PostListRepository {
  @override
  Future<PostListResponse> getPosts({int limit = 5, int offsetId}) async {
    return PostListRestDataProvider().getPostList(offsetId);
  }
}
