import 'package:fexper/dataproviders/rest/responses/post_list_response.dart';
import 'package:fexper/model/post.dart';
import 'package:fexper/repositories/post_list/abstract.dart';

class PostListRepositoryMock extends PostListRepository {
List<Post> _allPosts = [
      Post(id: 0, userId: 0, title: "FavoriteWidgetState", body: "The _FavoriteWidgetState class stores the mutable data that can change over the lifetime of the widget. When the app first launches, the UI displays a solid red star, indicating that the lake has “favorite” status, along with 41 likes. These values are stored in the _isFavorited and _favoriteCount fields", imageURL: "https://lh3.googleusercontent.com/proxy/MATocrGL7Y9c4232za2o0675yy5PSRW0jm6rWeOQLH1Mju7q7ZWjhiEbR-xATe2e6Hc5vX7EehxmVz_O6od6bv5LxU5-9N7RY-eSBLP3vTd7QCZeVMbH9JiBxT86e5DONBK_CGLF2H6tqFvJq5LvuAI"),
      Post(id: 1, userId: 1, title: "title2", body: "body2"),
      Post(id: 2, userId: 1, title: "title3", body: "body3"),
      Post(id: 3, userId: 2, title: "title4", body: "body4"),
      Post(id: 4, userId: 0, title: "title5", body: "body5"),
      Post(id: 5, userId: 0, title: "title6", body: "body6"),
      Post(id: 6, userId: 1, title: "title7", body: "body7"),
      Post(id: 7, userId: 1, title: "title8", body: "body8"),
      Post(id: 8, userId: 2, title: "title9", body: "body9"),
      Post(id: 9, userId: 0, title: "title10", body: "body10"),
      Post(id: 10, userId: 0, title: "title11", body: "body11"),
      Post(id: 11, userId: 1, title: "title12", body: "body12"),
      Post(id: 12, userId: 1, title: "title13", body: "body13"),
      Post(id: 13, userId: 2, title: "title14", body: "body14"),
      Post(id: 14, userId: 0, title: "title15", body: "body15"),
      Post(id: 15, userId: 0, title: "title16", body: "body16"),
      Post(id: 16, userId: 1, title: "title17", body: "body17"),
      Post(id: 17, userId: 1, title: "title18", body: "body18"),
      Post(id: 18, userId: 2, title: "title19", body: "body19"),
      Post(id: 19, userId: 0, title: "title20", body: "body20")
    ];

  @override
  Future<PostListResponse> getPosts({int limit = 5, int offsetId = -1}) async {
  
    await Future.delayed(Duration(milliseconds: 500));
    final list= _allPosts
        .where((value) => offsetId != null ? value.id > offsetId : true)
        .take(limit).toList();
    return PostListResponse(list: list, lastId: _allPosts.last.id);
  }
}
