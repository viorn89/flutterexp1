import 'package:fexper/dataproviders/rest/responses/post_list_response.dart';
import 'package:fexper/repositories/post_list/impl.dart';
import 'package:fexper/repositories/post_list/mock.dart';

abstract class PostListRepository {
  Future<PostListResponse> getPosts({int offsetId, int limit});

  static PostListRepository _instance;
  static PostListRepository instance() {
    if (_instance == null) {
      _instance = PostListRepositoryMock();
    }
    return _instance;
  }
}
