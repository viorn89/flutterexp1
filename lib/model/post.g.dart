// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

extension PostCopyWithExtension on Post {
  Post copyWith({
    String body,
    int id,
    String imageURL,
    String title,
    int userId,
  }) {
    return Post(
      body: body ?? this.body,
      id: id ?? this.id,
      imageURL: imageURL ?? this.imageURL,
      title: title ?? this.title,
      userId: userId ?? this.userId,
    );
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Post _$PostFromJson(Map<String, dynamic> json) {
  return Post(
    userId: json['userId'] as int,
    id: json['id'] as int,
    title: json['title'] as String,
    body: json['body'] as String,
    imageURL: json['imageURL'] as String,
  );
}

Map<String, dynamic> _$PostToJson(Post instance) => <String, dynamic>{
      'userId': instance.userId,
      'id': instance.id,
      'title': instance.title,
      'body': instance.body,
      'imageURL': instance.imageURL,
    };
