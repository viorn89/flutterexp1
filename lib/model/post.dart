import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'post.g.dart';

@immutable
@CopyWith()
@JsonSerializable(nullable: false)
class Post {
  final int userId, id;
  final String title, body, imageURL;
  Post({this.userId, this.id, this.title, this.body, this.imageURL});

  factory Post.fromJson(Map<String, dynamic> json) => _$PostFromJson(json);
  Map<String, dynamic> toJson() => _$PostToJson(this);
}
